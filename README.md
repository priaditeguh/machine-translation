# Machine Translation #

### Introduction ###

In this project, we build a deep neural network that functions as part of an end-to-end machine translation pipeline.The pipeline accept English text as input and return the French translation.

Our final model incorporates embedding, encoder-decoder, and bidirectional GRU, as shown in figure below. 

![Loss Plot](images/encoder_decoder.png)

### Dataset
We begin by investigating the dataset that will be used to train and evaluate your pipeline.  The most common datasets used for machine translation are from [WMT](http://www.statmt.org/).  However, that will take a long time to train a neural network on.  We'll be using a dataset we created for this project that contains a small vocabulary.  We'll be able to train your model in a reasonable time with this dataset.

### Results ###

- Training accuracy : 96.99 %
- Validation accuracy : 97.15 %